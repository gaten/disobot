from io import DEFAULT_BUFFER_SIZE

from discord import message
import patch
import traceback
import logging
import discord
from discord.ext import commands, tasks
from discord.ext.commands.cooldowns import BucketType
from datetime import datetime, timedelta, timezone, timedelta, time
from urllib import parse, request
import json, sys
import constants

## CONFIG ITEMS
config = None
creds = None

with open('config.json', 'r') as f:
    config = json.load(f)

with open('creds.json', 'r') as f:
    creds = json.load(f)

## setup logging
logger = logging.getLogger(__name__)  
logging.basicConfig(level=config['cLOGLEVEL'], format=config['cLOGFORMAT'])
file_handler = logging.FileHandler(config['cLOGFILE'])
formatter    = logging.Formatter(config['cLOGFORMAT'])
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

#intents = discord.Intents().all()
bot = commands.Bot(command_prefix='!', description="This is your future Overlord", owner_id=config['cADMIN'])
DEAFENED = False # Global which determines if the bot listens to anyone but the admin or not

HISTORY = []

# TODO: Put this in a cog
async def adminCheck(ctx):
    await ctx.message.add_reaction(constants.eROBOT)

    if commands.is_owner(): 
        await ctx.message.add_reaction(constants.eABACUS)
        return True
    else:
        await ctx.message.add_reaction(constants.eDENIED)
        
        # log attempt
        log = f'DENIED: {ctx.message.author}[{ctx.channel.type}]> {ctx.message.content}'
        logger.error(log)
        
        channel = await ctx.bot.fetch_user(bot.owner_id)
        await channel.send(log)

        return False

# great aync blog: https://saraswatmks.github.io/2020/05/asyncio-concurrent-python-tutorial.html
# list of discord checks: https://gist.github.com/Painezor/eb2519022cd2c907b56624105f94b190
## COMMANDS ##
@bot.command(pass_context = True, name = "history")
@commands.check(adminCheck)
async def history(ctx):
    """
    display the bot history
    """
    # returns command history of the bot
    # TODO: Last n commands
    msg = f'Command history as of {str(datetime.now())}\n'
    for command in HISTORY:
        msg = msg + command + '\n'

    await ctx.send(msg)

@bot.command(pass_context = True, name = "sssh")
@commands.check(adminCheck)
async def sssh(ctx):
    """
    deafen the bot
    """
    global DEAFENED
    # deafens the bot, so it will only respond to admins
    DEAFENED = not DEAFENED # toggle

    await ctx.send(f"Now DEAFENED = {DEAFENED}")

@bot.command(name = "load")
@commands.check(adminCheck)
@commands.cooldown(1, 30, BucketType.default) # Limit how often a command can be used, (num per, seconds, BucketType) 
@commands.max_concurrency(1, per=BucketType.default, wait=False)
async def load(ctx, extension):
    """
    load a cog
    """    
    try:
        bot.load_extension('cogs.' + extension)
        logger.info(f'Loaded {extension}')
        await ctx.send(f'Loaded {extension}')
    except Exception as error:
        await ctx.message.add_reaction(constants.eNO)
        logger.error(f"Extension '{extension}' could not be loaded.")
        logger.debug(f"Extension {extension} could not be loaded. [{error}]")

@bot.command(name = "reload")
@commands.check(adminCheck)
async def reload(ctx, extension):
    """
    reload a cog
    """
    try:
        bot.unload_extension('cogs.' + extension)
        bot.load_extension('cogs.' + extension)
        logger.info(f'Reloaded {extension}')
        await ctx.send(f'Reloaded {extension}')
    except Exception as error:
        await ctx.message.add_reaction(constants.eNO)
        logger.error(f"Extension {extension} could not be loaded.")
        logger.debug(f"Extension {extension} could not be reloaded. [{error}]")

@bot.command(name = "unload")
@commands.check(adminCheck)
async def unload(ctx, extension):
    """
    unload a cog
    """
    try:
        bot.unload_extension('cogs.' + extension)
        logger.debug(f'Unloaded {extension}')
        await ctx.send(f'{extension} successfully unloaded')
    except Exception as error:
        await ctx.message.add_reaction(constants.eNO)
        logger.error(f"Extension {extension} could not be loaded.")
        logger.debug(f"Extension {extension} could not be unloaded. [{error}]")

## EVENTS ##
@bot.event
async def on_error(event, *args, **kwargs):
    """Error handler for all events."""
    logger.debug(f'Ignoring exception in {event}\n{traceback.format_exc()}')

@bot.event
async def on_command_error(ctx: commands.Context, error: Exception):
    if hasattr(ctx.command, 'on_error'):
        return

    cog = ctx.cog
    if cog:
        if cog._get_overridden_method(cog.cog_command_error) is not None:
            return

    ignored = (commands.CommandNotFound, discord.HTTPException ) # These exceptions are ignored.
    error = getattr(error, 'original', error)

    if isinstance(error, ignored):
        return

    if isinstance(error, commands.CommandOnCooldown):
        await ctx.author.send(f'[{ctx.command}] Cool it! {error}')
        return

    if isinstance(error, commands.UserInputError):
        await ctx.author.send(f'[{ctx.command}] User input errror')
        return

    if isinstance(error, commands.CheckFailure):
        channel = await ctx.bot.fetch_user(290684958778064898)

        await ctx.message.add_reaction(constants.eDENIED)
        await channel.send(f'Command checks failed!')
        await channel.send(f'[{ctx.author}]> {ctx.command}')
        await ctx.author.send(f'[{ctx.command}] Command checks failed!')
        return

    if isinstance(error, commands.MaxConcurrencyReached):
        await ctx.author.send(f'[{ctx.command}] Bot is busy! Please retry in a minute')
        return

    if isinstance(error, commands.DisabledCommand):
        await ctx.send(f'{ctx.command} has been disabled.')
    elif isinstance(error, commands.NoPrivateMessage):
        try:
            await ctx.author.send(f'{ctx.command} can not be used in Private Messages.')
        except discord.HTTPException:
            pass
    elif isinstance(error, commands.BadArgument):
        if ctx.command.qualified_name == 'tag list':
            await ctx.send(f'[{ctx.command}] Bad Agurment. I could not find that member. Please try again.')
    else:
        logger.exception(f'Ignoring exception in command {ctx.command} {sys.stderr}')
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)    

@bot.event
async def on_ready(): 
    logger.info(f'My body is ready. Logged in as {bot.user.name} - {bot.user.id}: V {discord.__version__}')

async def secCheck():
    global DEAFENED
    return DEAFENED

@bot.event
async def on_message(message):
    global DEAFENED
    if message.author == bot.user:
        return
    if message.author.bot:
        return
    
    logline = f'{str(datetime.now())}: '
    if not message.guild:
        logline +=  f'[mid={message.id}] \
            [mentions={message.raw_mentions}] [author={message.author}] [author_id={message.author.id}] \
            [type={message.type}]: [content={message.content}]'
    else:
        logline +=  f'[mid={message.id}] \
            [guild={message.channel.guild}] [guild_id={message.channel.guild.id}] [channel={message.channel}] [channel_id={message.channel.id}] \
            [mentions={message.raw_mentions}] [author={message.author}] [author_id={message.author.id}] \
            [type={message.type}]: [content={message.content}]'

    logger.info(f'{message.author}> {message.content}')
    logger.debug(logline)
    HISTORY.append(logline)

    # admin only on PMs
    if (message.author.id != config['cADMIN']) and (not message.guild):
        logger.error(f'[-] INVALID ADMIN PM')
        return

    if DEAFENED and (message.author.id != config['cADMIN']):
        logger.error(f'[-] DEAFENED: Ignoring {logline}')
        # TODO: send admin alert
        return

    # only listen to non-admins on specific channels
    if (message.author.id != config['cADMIN']) and (message.channel.id != config['cCHANID']):
        logger.debug(f'[-] Ignoring: [{type(message.channel.id)}]{message.author}: {message.content}')
        return
    
    await bot.process_commands(message)

cogs = ["cogs.S4", "cogs.TestCog", "cogs.TaskCog"]

## MAIN ##
if __name__ == "__main__":
    for cog in cogs:                          
        try:                                  
            bot.load_extension(cog)           
            logger.info(f'{cog} was loaded.') 
        except Exception as e:    
            logger.error(f'Failed to load Cog {cog}. {e}')

    bot.run(creds['token'])