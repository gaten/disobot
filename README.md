# Disobot

Requirements:
* python3
* discord.py
* beautifulsoup4
* numpy
* urllib

Simple discord bot using the new Cogs system.


1. Create a file named `creds.json`:

```
{
    "token":"[DISCORD BOT TOKEN]"
}
```

2. Edit `config.json` and replace `cCHANID` with a channel ID the bot should be restricted to and `cADMIN` and as the owner/admin of the bot.

python3 main.py
