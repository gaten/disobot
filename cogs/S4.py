# -*- coding: utf-8 -*-
"""
Created on Tue May  4 10:22:39 2021

@author: BPAPP
"""
import requests
from bs4 import BeautifulSoup

import re
from datetime import datetime, timedelta, timezone, timedelta, time, date 
import pandas as pd

import discord
from discord.ext import commands, tasks
import constants
import logging

tz = timezone(timedelta(hours=-5)) # EST

when = [
    time(21, 27, tzinfo=tz), # 2am 
    time(21, 25, tzinfo=tz),   # 9am
    time(9,  0, tzinfo=tz),   # 7am
]

class S4(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None
        self.logger = logging.getLogger(__name__)  
        logging.basicConfig(level=constants.cLOGLEVEL, format=constants.cLOGFORMAT)
        self.latestS4 = {}
        self.gottem.start()


    @tasks.loop(time=when)
    async def gottem(self):
        logging.info("Running gottem()")

        channel = self.bot.get_channel(782455080313028608) 

        embed=discord.Embed(title=f'S4 Daily Scrape: {str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))}', url="https://www.sec.gov/cgi-bin/current?q1=2&q2=5&q3=S-4", description="sec.gov S4 Query performed daily at 8AM EST", color=0xFF5733)
        s = self.getS4Links()
        for url in s['URL']:
            embed.add_field(name="URL", value=f'{url}', inline=False)

        self.logger.info("S4 end()")
        await channel.send(embed=embed)

    @commands.command()
    async def s4(self, ctx):
        """
        grab the latest S4 EDGAR events
        """
        await ctx.message.add_reaction(constants.eROBOT)

        current_dt = datetime.now().strftime("%Y-%m-%d") # laziest cache ever
        if current_dt not in self.latestS4:
            embed=discord.Embed(title=f'S4 Daily Scrape: {str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))}', url="https://www.sec.gov/cgi-bin/current?q1=2&q2=5&q3=S-4", description="sec.gov S4 Query performed daily at 8AM EST", color=0xFF5733)
            s = self.getS4Links()
            for url in s['URL']:
                embed.add_field(name="URL", value=f'{url}', inline=False)
        
            self.latestS4[current_dt] = embed.copy()
        
        await ctx.send(embed=self.latestS4[current_dt])

    @commands.Cog.listener()
    async def on_ready(self):
        print('cog ready')
        self.logger.info('cog is ready!')

    def getS4Links(self):
        endpoint = r"https://www.sec.gov/cgi-bin/current?q1=2&q2=5&q3=S-4"
        response = requests.get(url = endpoint)
        soup = BeautifulSoup(response.content, 'html.parser')

        # find the document table with our data
        doc_table = soup.find_all('S-4', class_='tableFile2')

        link_dic    = {'LINK': []}
        output      = {'URL': [],
                    'CIK': [],
                    'FILE_DATE': []}
        today       = date.today()
        dateStr     = today. strftime("%d %b %Y ")
        df_output   = pd.DataFrame()
        
        # GET ALL LINKS
        for link in soup.find_all('a'):
            link = link.get('href')
            m = re.search(r'\d+$', link)
            # IF 1st 
            if link.startswith("/"):
                links = "https://www.sec.gov"+link
            else :
                links = "https://www.sec.gov/"+link
            
            if links.endswith('index.html'):
                url = links
                dt = dateStr
                output["URL"].append(links)
                output["FILE_DATE"].append(dateStr)
            elif m is not None:
                cik = link[-7:]
                output["CIK"].append(link[-7:])
                
                df = pd.DataFrame({"URL":[url],"CIK":[cik],"DATE":[dt]})
                df_output = df_output.append(df)
        return output    

def setup(bot):
    bot.add_cog(S4(bot))