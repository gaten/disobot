from discord.ext import tasks, commands
from datetime import datetime, timedelta, timezone, timedelta, time

tz = timezone(timedelta(hours=-7)) # PST

when = [
    time(22, 25, tzinfo=tz), # 8pm 
    time(0, 0, tzinfo=tz),   # midnight
    time(1, 8, tzinfo=tz),   # 5am
]

class TaskCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.index = 0
        #self.printer.start()

    def cog_unload(self):
        pass
        self.printer.cancel()

    @tasks.loop(seconds=2.0)
    async def printer(self):
        print(f'Index: {self.index}')

def setup(bot):
    bot.add_cog(TaskCog(bot))