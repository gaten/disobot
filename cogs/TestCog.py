import discord
from discord import channel
from discord.ext import commands, tasks
from discord.ext.commands.cooldowns import BucketType
from datetime import datetime, timedelta, timezone, timedelta, time
import asyncio
import logging
import urllib
import json
import constants


class TestCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None
        self.logger = logging.getLogger(__name__)  
        self.index = 0
        self.lock = asyncio.Lock()
        self.printer.start()
        self.currentXKCD = {}

    def cog_unload(self):
        self.printer.cancel()
        return super().cog_unload()

    ### LOOPS
    @tasks.loop(seconds=5.0)
    async def printer(self):
        self.logger.debug(f"During Tracker loop {self.index}")
        self.index += 1        

    @printer.before_loop
    async def before_printer(self):
        self.logger.info(f'Before tracker loop {self.index}')
        await self.bot.wait_until_ready()

    @printer.after_loop
    async def after_printer(self):
        self.logger.info(f'After tracker loop {self.index}')

    ### COMMANDS
    @commands.command(name="sadmin", description = 'Send the admin a message')
    @commands.cooldown(1, 30, BucketType.default)
    @commands.max_concurrency(2, per=BucketType.default, wait=False)
    async def sadmin(self, ctx, msg: str = None):
        """
        send the bot admin a message
        """
        channel = await self.bot.fetch_user(self.bot.owner_id)
        await channel.send(f'{msg}')

    @commands.command(name="ping", description = 'ping pong!')
    @commands.cooldown(1, 60, BucketType.default)
    @commands.max_concurrency(1, per=BucketType.default, wait=False)
    async def ping(self, ctx):
        """
        ping pong!
        """
        await ctx.message.add_reaction(constants.eROBOT)
        await ctx.reply(f"`pong:{round(self.bot.latency,2)}`")

    @commands.command(pass_context = True, name="info")
    async def info(self, ctx):
        """
        get info about the bot
        """
        embed = discord.Embed(title=f"{ctx.bot.user.name}", description="Info command", timestamp=datetime.utcnow(), color=discord.Color.blue())
        
        if ctx.guild is not None:
            embed.add_field(name="Server Name", value=f"{ctx.guild.name}")
            embed.add_field(name="Server Creation", value=f"{ctx.guild.created_at}")
            embed.add_field(name="Server Owner", value=f"{ctx.guild.owner}")
            embed.add_field(name="Server Region", value=f"{ctx.guild.region}")
            embed.add_field(name="Server ID", value=f"{ctx.guild.id}")

        embed.add_field(name="User name", value=f"{ctx.bot.user.name}")
        embed.add_field(name="User ID", value=f"{ctx.bot.user.id}")
        embed.add_field(name="Owner ID", value=f"{ctx.bot.owner_id}")
        embed.add_field(name="Your ID", value=f"{ctx.author.id}")
        embed.add_field(name="Discord version", value=f"{discord.__version__}")

        await ctx.author.send(embed=embed) # DM back to author

     # credit: https://github.com/jcsumlin/Peribot/blob/master/cogs/xkcd.py
    @commands.command(name="xkcd")
    async def xkcd(self, ctx, number: int = None):
        """
        get the current XKCD comic
        :param number: Comic number
        :return: The comic you requested
        """
        apiurl = "http://xkcd.com/{x}/info.0.json"
        current = "http://xkcd.com/info.0.json"

        current_dt = datetime.now().strftime("%Y-%m-%d")
        if not current_dt in self.currentXKCD:
            if number is None:
                try:
                    # with urllib.request.urlopen(self.apiurl.format(x=number)) as url: # for numbered comic
                    with urllib.request.urlopen(current) as url:
                        data = json.loads(url.read().decode())
                        embed = discord.Embed(title=f'{data["title"]} #{data["num"]} ')
                        embed.set_image(url=data["img"])
                        embed.set_footer(text=f"{data['alt']}")
                except:
                    await ctx.send("Could not find an XKCD with this ID!")

            # update "cache"
            self.logger.info("Updating xkcd cache")                
            self.currentXKCD[current_dt] = embed.copy()
        
        await ctx.send(embed=self.currentXKCD[current_dt])

    def myCheck(self):
        return False

    @commands.is_owner()    # 
    @commands.cooldown(1, 30, BucketType.default) # Limit how often a command can be used, (num per, seconds, BucketType) 
    @commands.max_concurrency(1, per=BucketType.default, wait=False) # Limit how many people can run the command at once
    @commands.check(myCheck)    # custom check
    @commands.command(name="track")
    async def track(self, ctx):
        """
        check the async tracker
        """
        self.logger.info("TRACKER")
        await ctx.send(f'Index: {self.index}')
    
    ## listener
    @commands.Cog.listener()
    async def on_ready(self):
        self.logger.info(f'{__name__} cog is ready!')

def setup(bot):
    bot.add_cog(TestCog(bot))
