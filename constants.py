from datetime import datetime, timedelta, timezone, timedelta, time
import logging

## EMOJIS ##
eTHUMBSUP = '\N{THUMBS UP SIGN}'
eABACUS = '\U0001F9EE'
eROBOT = '\U0001F916' # '\N{ROBOT}'
eWARNING = '\U000026A0' #'\U000026A0'
eNO = '\U000026D4'
eDENIED = '\U00001F510'

## LOGGING ##
cLOGLEVEL = logging.INFO
cLOGFORMAT = '%(asctime)s : %(levelname)s : %(funcName)s : %(message)s'

#tz = timezone(timedelta(hours=-5)) # EST
#tz = timezone(timedelta(hours=-7)) # PST