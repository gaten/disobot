from discord.ext import commands, tasks
from datetime import datetime, timedelta, timezone, timedelta, time

# had to overrider because they forgot an if statement
def override_prepare_time_index(self, now=None):
    # now kwarg should be a datetime.datetime representing the time "now"
    # to calculate the next time index from

    # pre-condition: self._time is set
    time_now = (now or datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0)).timetz()
    if self._time: 
        for idx, time in enumerate(self._time):
            if time >= time_now:
                self._time_index = idx
                break
    else:
        self._time_index = 0

tasks.Loop._prepare_time_index = override_prepare_time_index